import {exp} from 'react-native-reanimated';
import DMAvatarDummy from './avatar-dummy.png';
import DMDoctor1 from './doctor-1.png';
import DMDoctor2 from './doctor-2.png';
import DMDoctor3 from './doctor-3.png';
import DMNewsPic1 from './news-pic-1.png';
import DMNewsPic2 from './news-pic-2.png';
import DMNewsPic3 from './news-pic-3.png';
import DMHospitalPic1 from './hospital-1.png';
import DMHospitalPic2 from './hospotal-2.png';
import DMHospitalPic3 from './hospital-3.png';

export {
  DMAvatarDummy,
  DMNewsPic3,
  DMNewsPic2,
  DMNewsPic1,
  DMDoctor3,
  DMDoctor2,
  DMDoctor1,
  DMHospitalPic1,
  DMHospitalPic2,
  DMHospitalPic3,
};
