import ILLogoRounded from './logo-dokterbaik1.svg';
import ILBGGetStarted from './getstarted-background.png';
import ILWCGetStarted from './getstarted-welcome.svg';
import ILLogoClear from './logo-dokterbaik2.svg';
import ILWCSignIn from './signin-welcome.svg';
import ILPhotoDummy from './photo-dummy.png';
import ILBGNearbyHospital from './nearbyhospital-background.jpg';

export {
  ILLogoRounded,
  ILBGGetStarted,
  ILLogoClear,
  ILWCGetStarted,
  ILWCSignIn,
  ILPhotoDummy,
  ILBGNearbyHospital,
};
