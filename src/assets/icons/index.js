import ICBackDark from './back-dark.svg';
import ICBackLight from './back-light.svg';
import ICPhotoAdd from './photo-add.svg';
import ICPhotoRemove from './photo-remove.svg';
import ICHome from './home.svg';
import ICMessages from './message.svg';
import ICHospital from './hospital3.svg';
import ICPediatrician from './doctor-pediatrician.svg';
import ICGeneralPractitioner from './doctor-general practitioners.svg';
import ICPsychiatrist from './doctor-psychiatrist.svg';
import ICDoctorObat from './dokter-obat.svg';
import ICStar from './star-2.svg';
import ICStar2 from './star.svg';

export {
  ICBackDark,
  ICBackLight,
  ICPhotoAdd,
  ICHospital,
  ICPhotoRemove,
  ICHome,
  ICMessages,
  ICDoctorObat,
  ICPediatrician,
  ICPsychiatrist,
  ICGeneralPractitioner,
  ICStar,
  ICStar2,
};
