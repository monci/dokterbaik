const mainColors = {
  primary: '#0BCAD4',
  secondary: '#56DF79',
  blue1: '#112340',
  grey1: '#D9D9D9',
  grey2: '#7D8797',
};

export const colors = {
  primary: mainColors.primary,
  secondary: mainColors.secondary,
  black: 'black',
  white: 'white',
  text: {
    primary: mainColors.blue1,
    secondary: mainColors.grey2,
  },
  button: {
    primary: {
      background: mainColors.primary,
      text: 'white',
    },
    secondary: {
      background: mainColors.secondary,
      text: 'white',
    },
  },
  border: mainColors.grey1,
  blue1: mainColors.blue1,
};
