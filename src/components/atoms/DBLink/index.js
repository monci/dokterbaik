import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const DBLink = ({label, fontSize, textAlign}) => {
  return (
    <View>
      <Text style={styles.text(fontSize, textAlign)}>{label}</Text>
    </View>
  );
};

export default DBLink;

const styles = StyleSheet.create({
  text: (fontSize, textAlign) => ({
    fontSize: fontSize,
    color: colors.text.secondary,
    fontFamily: fonts.primary[400],
    textDecorationLine: 'underline',
    textAlign: textAlign,
  }),
});
