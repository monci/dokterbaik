import React from 'react';
import {StyleSheet, View} from 'react-native';

const DBGap = ({height, width}) => {
  return <View style={{height: height, width: width}} />;
};

export default DBGap;

const styles = StyleSheet.create({});
