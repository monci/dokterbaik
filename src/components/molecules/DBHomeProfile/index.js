import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {DMAvatarDummy} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DBHomeProfile = () => {
  return (
    <View style={styles.container}>
      <Image source={DMAvatarDummy} style={styles.avatar} />
      <View>
        <Text style={styles.name}>Miftahul Bagus Pranoto</Text>
        <Text style={styles.email}>miftahulbagus@gmail.com</Text>
      </View>
    </View>
  );
};

export default DBHomeProfile;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: colors.primary,
    borderRadius: 10,
    alignItems: 'center',
  },
  avatar: {width: 46, height: 46, borderRadius: 46 / 2, marginRight: 12},
  name: {
    fontSize: 18,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  email: {
    fontSize: 13,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
});
