import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {DMDoctor1, ICStar2} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DBRatedDoctor = () => {
  return (
    <View style={styles.container}>
      <Image source={DMDoctor1} style={styles.avatar} />
      <View style={styles.profile}>
        <Text style={styles.name}>Najwa Zain</Text>
        <Text style={styles.category}>General Practitioner</Text>
      </View>
      <View style={styles.rating}>
        <ICStar2 />
        <ICStar2 />
        <ICStar2 />
        <ICStar2 />
        <ICStar2 />
      </View>
    </View>
  );
};

export default DBRatedDoctor;

const styles = StyleSheet.create({
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    marginRight: 12,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 16,
  },
  rating: {
    flexDirection: 'row',
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
  },
  category: {
    fontFamily: fonts.primary.normal,
    fontSize: 12,
    color: colors.text.secondary,
    marginTop: 2,
  },
  profile: {
    flex: 1,
  },
});
