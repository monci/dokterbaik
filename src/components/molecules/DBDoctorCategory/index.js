import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  ICDoctorObat,
  ICGeneralPractitioner,
  ICPediatrician,
  ICPsychiatrist,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DBDoctorCategory = ({category}) => {
  const Icon = () => {
    if (category === 'General Practitioners') {
      return <ICGeneralPractitioner style={styles.icon} />;
    } else if (category === 'Psychiatrist') {
      return <ICPsychiatrist style={styles.icon} />;
    } else if (category === 'Pediatrician') {
      return <ICPediatrician style={styles.icon} />;
    } else {
      return <ICDoctorObat style={styles.icon} />;
    }
    // return <ICGeneralPractitioner style={styles.icon} />;
  };
  return (
    <View style={styles.container}>
      <Icon />
      <Text style={styles.category}>{category}</Text>
    </View>
  );
};

export default DBDoctorCategory;

const styles = StyleSheet.create({
  container: {
    padding: 12,
    backgroundColor: '#1abc9c',
    alignSelf: 'flex-start',
    borderRadius: 10,
    marginRight: 10,
    width: 100,
    height: 130,
  },
  icon: {
    marginBottom: 28,
  },
  category: {
    fontSize: 13,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
    maxWidth: 80,
  },
});
