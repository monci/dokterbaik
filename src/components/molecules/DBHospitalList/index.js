import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {DMHospitalPic1} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DBHospitalList = ({pic, name, address, phone}) => {
  return (
    <View style={styles.container}>
      <Image source={pic} style={styles.picture} />
      <View>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.address}>{address}</Text>
        <Text style={styles.phone}>{phone}</Text>
      </View>
    </View>
  );
};

export default DBHospitalList;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
  },
  picture: {
    width: 75,
    height: 75,
    borderRadius: 12,
    marginRight: 16,
    resizeMode: 'cover',
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    maxWidth: 275,
  },
  address: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.secondary,
    marginTop: 5,
    maxWidth: 275,
  },
  phone: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.secondary,
    marginTop: 5,
  },
});
