import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {DBButton, DBGap} from '../../atoms';

const DBHeader = ({title, onPress}) => {
  return (
    <View style={styles.container}>
      <DBButton type="icon-only" icon="back-dark" onPress={onPress} />
      <Text style={styles.text}>{title}</Text>
      <DBGap width={24} />
    </View>
  );
};

export default DBHeader;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    flex: 1,
    textAlign: 'center',
    fontFamily: fonts.primary[700],
    fontSize: 20,
    color: colors.text.primary,
  },
});
