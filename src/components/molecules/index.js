import DBHeader from './DBHeader';
import DBHomeProfile from './DBHomeProfile';
import DBDoctorCategory from './DBDoctorCategory';
import DBRatedDoctor from './DBRatedDoctor';
import DBNewsItem from './DBNewsItem';
import DBDoctorList from './DBDoctorList';
import DBHospitalList from './DBHospitalList';

export {
  DBHeader,
  DBHomeProfile,
  DBNewsItem,
  DBDoctorCategory,
  DBRatedDoctor,
  DBDoctorList,
  DBHospitalList,
};
