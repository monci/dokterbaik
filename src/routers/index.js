import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Splash,
  GetStarted,
  SignUp,
  SignIn,
  UploadPhoto,
  Home,
  Messages,
  Hospitals,
} from '../pages';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {AnimatedTabBarNavigator} from 'react-native-animated-nav-tab-bar';
import {colors} from '../utils';
import {ICHome, ICHospital, ICMessages} from '../assets';

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();
const Tabs = AnimatedTabBarNavigator();

// const MainApp = () => {
//   return (
//     <Tab.Navigator initialRouteName="Home">
//       <Tab.Screen name="Hospital" component={Hospitals} />
//       <Tab.Screen name="Home" component={Home} />
//       <Tab.Screen name="Messages" component={Messages} />
//     </Tab.Navigator>
//   );
// };

const MainApp = () => {
  return (
    <Tabs.Navigator
      initialRouteName="Home"
      // default configuration from React Navigation
      appearence={{
        tabBarBackground: '#2c3e50',
        activeColors: colors.black,
        floating: true,
        dotSize: 'small',
      }}
      tabBarOptions={{
        activeTintColor: colors.secondary,
        inactiveTintColor: colors.white,
        // activeBackgroundColor: colors.primary,
      }}>
      <Tabs.Screen
        name="Hospitals"
        component={Hospitals}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <ICHospital
              name="hospital"
              size={size ? size : 1}
              color={focused ? color : '#222222'}
              focused={focused}
              color={color}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <ICHome
              name="home"
              size={size ? size : 24}
              color={focused ? color : '#222222'}
              focused={focused}
              color={color}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Messages"
        component={Messages}
        options={{
          tabBarIcon: ({focused, color, size}) => (
            <ICMessages
              name="messages"
              size={size ? size : 24}
              color={focused ? color : '#222222'}
              focused={focused}
              color={color}
            />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

const Routers = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="GetStarted"
        component={GetStarted}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="UploadPhoto"
        component={UploadPhoto}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Routers;
