import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {DMDoctor1, DMDoctor2, DMDoctor3} from '../../assets';
import {DBDoctorList} from '../../components';
import {colors, fonts} from '../../utils';

const Messages = () => {
  const [doctors, setDoctors] = useState([
    {
      id: 1,
      pic: DMDoctor1,
      name: 'Dr. Bagus',
      desc: 'ya boss...',
    },
    {
      id: 2,
      pic: DMDoctor2,
      name: 'Dr. Boyke',
      desc: 'ya bossku...',
    },
    {
      id: 3,
      pic: DMDoctor3,
      name: 'Dr. Pranoto',
      desc: 'Makanya boss...',
    },
  ]);
  return (
    <View style={styles.page}>
      <Text style={styles.title}>Messages</Text>
      {doctors.map((doctor) => {
        return (
          <DBDoctorList
            key={doctor.id}
            avatar={doctor.pic}
            name={doctor.name}
            desc={doctor.desc}
          />
        );
      })}
    </View>
  );
};

export default Messages;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 30,
    marginLeft: 16,
  },
});
