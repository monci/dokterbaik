import React from 'react';
import {Image, StyleSheet, View, Text} from 'react-native';
import {ICPhotoAdd, ILPhotoDummy} from '../../assets';
import {DBButton, DBGap, DBHeader, DBLink} from '../../components';
import {colors, fonts} from '../../utils';

const UploadPhoto = ({navigation}) => {
  return (
    <View style={styles.page}>
      <DBHeader title="Upload Photo" onPress={() => navigation.goBack()} />
      <View style={styles.content}>
        <View style={styles.profile}>
          <View style={styles.avatarWrapper}>
            <Image style={styles.avatar} source={ILPhotoDummy} />
            <ICPhotoAdd style={styles.addPhoto} />
          </View>
          <Text style={styles.name}>Miftahul Bagus</Text>
          <Text style={styles.email}>miftahulbagus@gmail.com</Text>
        </View>
        <View>
          <DBButton title="Upload and Continue" />
          <DBGap height={30} />
          <DBLink label="Skip for this" textAlign="center" fontSize={16} />
        </View>
      </View>
    </View>
  );
};

export default UploadPhoto;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white},
  avatar: {width: 110, height: 110},
  avatarWrapper: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    borderWidth: 1,
    color: colors.border,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addPhoto: {
    position: 'absolute',
    bottom: 8,
    right: 6,
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 24,
    color: colors.text.primary,
    textAlign: 'center',
  },
  email: {
    fontFamily: fonts.primary.normal,
    fontSize: 16,
    color: colors.text.secondary,
    marginTop: 4,
    textAlign: 'center',
  },
  content: {
    paddingHorizontal: 30,
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: 60,
  },
  profile: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
