import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import DBHeader from '../../components/molecules/DBHeader';
import {DBButton, DBGap, DBInput} from '../../components/atoms';
import {colors} from '../../utils';

const SignUp = ({navigation}) => {
  return (
    <View style={styles.page}>
      <DBHeader title="Sign Up" onPress={() => navigation.goBack()} />
      <ScrollView>
        <View style={styles.content}>
          <DBInput label="Full Name" />
          <DBGap height={24} />
          <DBInput label="Date of Birth" />
          <DBGap height={24} />
          <DBInput label="Email Address" />
          <DBGap height={24} />
          <DBInput label="Phone Number" />
          <DBGap height={24} />
          <DBInput label="Password" />
          <DBGap height={24} />
          <DBInput label="Confirm Password" />
          <DBGap height={40} />
          <DBButton
            title="Continue"
            onPress={() => navigation.navigate('UploadPhoto')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white},
  content: {padding: 30, paddingTop: 0, backgroundColor: colors.white},
});
