import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ILLogoClear, ILWCSignIn} from '../../assets';
import {DBButton, DBGap, DBInput, DBLink} from '../../components/atoms';
import DBHeader from '../../components/molecules/DBHeader';
import {colors} from '../../utils';

const SignIn = ({navigation}) => {
  return (
    <View style={styles.page}>
      <DBHeader
        title="Sign In"
        icon="back-dark"
        onPress={() => navigation.goBack()}
      />
      <View style={styles.content}>
        <ILLogoClear />
        <ILWCSignIn style={styles.welcomeTitle} />
        <DBInput label="Email Address" />
        <DBGap height={24} />
        <DBInput label="Password" />
        <DBGap height={10} />
        <DBLink label="Forgot Password?" fontSize={13} />
        <DBGap height={40} />
        <DBButton
          title="Sign In"
          onPress={() => navigation.replace('MainApp')}
        />
        <DBGap height={30} />
        <DBLink label="Create New Account" fontSize={16} textAlign="center" />
      </View>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: colors.white},
  content: {
    paddingHorizontal: 30,
    flex: 1,
    backgroundColor: colors.white,
  },
  welcomeTitle: {marginTop: 40, marginBottom: 40},
});
