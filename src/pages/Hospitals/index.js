import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {
  DMHospitalPic1,
  DMHospitalPic2,
  DMHospitalPic3,
  ILBGNearbyHospital,
} from '../../assets';
import {DBHospitalList} from '../../components';
import {colors, fonts} from '../../utils';

const Hospitals = () => {
  return (
    <View style={styles.page}>
      <ImageBackground source={ILBGNearbyHospital} style={styles.background}>
        <Text style={styles.title}>Nearby Hospitals</Text>
        <Text style={styles.availability}>3 available</Text>
        <View style={styles.content}>
          <DBHospitalList
            pic={DMHospitalPic1}
            name="Mitra Keluarga Gading Serpong"
            address="Jl. Raya Legok - Karawaci No.20, Medang, Kec. Pagedangan, Tangerang, Banten 15810"
            phone="(021) 55689111"
          />
          <DBHospitalList
            pic={DMHospitalPic2}
            name="Eka Hospital BSD"
            address="Central Business District Lot. IX, Jl. Boulevard BSD Tim., Lengkong Gudang, Kec. Serpong, Kota Tangerang Selatan, Banten 15321"
            phone="(021) 25655555"
          />
          <DBHospitalList
            pic={DMHospitalPic3}
            name="St. Carolus Hospital Summarecon Serpong"
            address="Jl. Gading Golf Boulevard Kav. 08, Gading Serpong, Cihuni, Kec. Pagedangan, Tangerang, Banten 15332"
            phone="(021) 54220811"
          />
        </View>
      </ImageBackground>
    </View>
  );
};

export default Hospitals;

const styles = StyleSheet.create({
  page: {flex: 1, borderRadius: 20},
  content: {
    borderRadius: 25,
    flex: 1,
    backgroundColor: colors.white,
    marginTop: 150,
    paddingTop: 10,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    paddingTop: 20,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.white,
    textAlign: 'center',
  },
  availability: {
    fontSize: 14,
    fontFamily: fonts.primary[300],
    color: colors.white,
    textAlign: 'center',
  },
});
