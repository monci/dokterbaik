import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {JSONDoctorCategory} from '../../assets';
import {
  DBDoctorCategory,
  DBGap,
  DBHomeProfile,
  DBNewsItem,
  DBRatedDoctor,
} from '../../components';
import {colors, fonts} from '../../utils';

const Home = () => {
  return (
    <View style={styles.page}>
      <DBGap height={30} />
      <View style={styles.wrapperSection}>
        <DBHomeProfile />
      </View>
      <DBGap height={7} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrapperSection}>
          <Text style={styles.welcomeText}>
            Who do you want to consult with?
          </Text>
        </View>
        <View style={styles.wrapperScroll}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.doctorCategory}>
              <DBGap width={32} />
              {JSONDoctorCategory.data.map((item) => {
                return (
                  <DBDoctorCategory key={item.id} category={item.category} />
                );
              })}
              <DBGap width={22} />
            </View>
          </ScrollView>
        </View>
        <View style={styles.wrapperSection}>
          <Text style={styles.sectionLabel}>Top Rated Doctor</Text>
          <DBRatedDoctor />
          <DBRatedDoctor />
          <DBRatedDoctor />
          <Text style={styles.sectionLabel}>News</Text>
          <DBNewsItem />
          <DBNewsItem />
          <DBNewsItem />
          <DBGap height={30} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  welcomeText: {
    fontSize: 20,
    marginTop: 10,
    marginBottom: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    maxWidth: 200,
  },
  doctorCategory: {flexDirection: 'row'},
  wrapperScroll: {marginHorizontal: -16},
  sectionLabel: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 20,
    marginBottom: 16,
  },
  wrapperSection: {
    paddingHorizontal: 16,
  },
});
