import React from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import {ILBGGetStarted, ILLogoClear, ILWCGetStarted} from '../../assets';
import {DBButton, DBGap} from '../../components/atoms';

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={ILBGGetStarted} style={styles.page}>
      <View>
        <ILLogoClear />
        <ILWCGetStarted style={styles.welcomeTitle} />
      </View>
      <View>
        <DBButton
          type="primary"
          title="Get Started"
          onPress={() => navigation.navigate('SignUp')}
        />
        <DBGap height={16} />
        <DBButton
          type="secondary"
          title="Sign In"
          onPress={() => navigation.navigate('SignIn')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  page: {padding: 30, justifyContent: 'space-between', flex: 1},
  welcomeTitle: {marginTop: 215},
});
