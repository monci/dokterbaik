import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ILLogoRounded} from '../../assets';
import {colors} from '../../utils';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('GetStarted');
    }, 3000);
  }, []);
  return (
    <View style={styles.logo}>
      <ILLogoRounded />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  logo: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
});
