import Splash from './Splash';
import GetStarted from './GetStarted';
import SignUp from './SignUp';
import SignIn from './SignIn';
import UploadPhoto from './UploadPhoto';
import Home from './Home';
import Hospitals from './Hospitals';
import Messages from './Messages';

export {
  Splash,
  GetStarted,
  SignIn,
  SignUp,
  UploadPhoto,
  Home,
  Messages,
  Hospitals,
};
