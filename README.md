DokterBaik is an application that I want to develop to connect good doctors in Indonesia with patients in need. I will build this application using react-native as the mobile app, and firebase as the backend.

Progress :

![picture](screenshoot/splash-screen.png)

![picture](screenshoot/welcome-screen.png)

![picture](screenshoot/sign-up.png)

![picture](screenshoot/upload-photo.png)

![picture](screenshoot/sign-in.png)

![picture](screenshoot/home.png)

![picture](screenshoot/hospitals.png)

![picture](screenshoot/messages.png)